<?php

namespace App\Controller;

use App\Form\UloginRegisterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UloginController extends AbstractBaseController
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/social-auth-back", name="social_auth_back")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @param ObjectManager $manager
     * @return Response
     */
    public function socialAuthBackAction(
        Request $request,
        UserRepository $userRepository,
        ApiContext $apiContext,
        UserHandler $userHandler,
        ObjectManager $manager
    )
    {
        try {

            if (!$request->isMethod("POST")) {
                throw  new ApiException("Неверный метод отпраки данных.");
            }

            $uloginData = $this->getUloginData($request);

            if (!$uloginData) {
                throw  new ApiException("Ulogin не прислал данные.");
            }

            $this->get('session')->set(self::ULOGIN_DATA, $uloginData);

            $uid = $uloginData['uid'];
            $network = $uloginData['network'];

            $user = $userRepository->findOneByUid($uid, $network);

            if ($user) {
                $userHandler->signIn($user);
                return $this->redirectToRoute("authorization_successful");
            }

            $client = $apiContext->uloginClientExists($uid, $network);

            if (!$client) {
                return $this->redirectToRoute("user_register_through");
            }

            $clientData = $apiContext->getUloginDataClient($uid, $network);

            if (!$clientData) {
                throw  new ApiException("Не придвиденная ошибка.");
            }

            $userData = $userRepository->findOneByPassportAndEmail(
                $clientData['passport'],
                $clientData['email']
            );

            if ($userData) {
                $userData->setVkId($clientData['vkId']);
                $userData->setFaceBookId($clientData['faceBookId']);
                $userData->setGoogleId($clientData['googleId']);
            } else {
                $userData = $userHandler->createNewUser($clientData);
            }

            $manager->persist($userData);
            $manager->flush();

            $userHandler->signIn($userData);
            return $this->redirectToRoute("authorization_successful");


        } catch (ApiException $e) {
            $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            return new Response($error);
        }
    }

    /**
     * @Route("/user-register-through", name="user_register_through")
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     */
    public function userRegisterThrough(
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager,
        Request $request)
    {
        $userData = $this->get('session')->get(self::ULOGIN_DATA);

        $network = $userData['network'];
        $uid = $userData['uid'];

        $user = $userHandler->createNewUser([
            'email' => $userData['email'],
            'passport' => '',
            'password' => "secret-password"
        ]);

        $user->setNetworkUlogin($network, $uid);

        $form = $this->createForm(
            ULoginRegisterType::class,
            $user
        );

        $form->handleRequest($request);

        $error = null;

        if ($form->isSubmitted() && $form->isValid()) {

            try {

                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    throw new ApiException('Такой E-mail или Паспорт уже есть в системе.');
                }

                $data = [
                    'email' => $user->getEmail(),
                    'uid' => $uid,
                    'passport' => $user->getPassport(),
                    'network' => $network,
                    'password' => $user->getPassword(),
                    'roles' => $user->getRoles(),
                ];

                $apiContext->createClientSoc($data);

                $manager->persist($user);
                $manager->flush();

                $userHandler->signIn($user);

                return $this->redirectToRoute("authorization_successful");

            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            }
        }

        return $this->render('ulogin/user_register_through.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
            'network' => $network
        ]);
    }

}