<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractBaseController extends Controller
{
    /**
     * @param Request $request
     * @return array|bool
     */
    public function getUloginData(Request $request)
    {
        $host = $request->getHost();
        $token = $request->get("token");
        $user = file_get_contents('http://ulogin.ru/token.php?token=' . $token . '&host=' . $host);
        $data = json_decode($user, true);

        if (!$data) {
            return false;
        }

        $result['email'] =  $data['email'] ?? null;
        $result['uid'] = $data['uid'];
        $result['network'] = $data['network'];

        return $result;
    }
}