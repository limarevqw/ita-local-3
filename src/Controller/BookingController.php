<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\BookingObjectType;
use App\Form\CottageType;
use App\Form\PensionType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthController
 * @package App\Controller
 * @Route("booking")
 */
class BookingController extends AbstractBaseController
{
    /**
     * @Route("/", name="app_booking_index")
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('booking/index.html.twig');
    }

    /**
     * @Route("/show/{id}", name="app_booking_show")
     * @param ApiContext $apiContext
     * @param int $id
     * @return Response
     */
    public function showAction(ApiContext $apiContext, int $id)
    {
        $error = null;
        $bookingObject = [];

        try {
            $result = $apiContext->getOneBookingObject($id);
            $bookingObject = $result['result'][0];
        } catch (ApiException $e) {
            $error = $e->getMessage();
        }

        return $this->render('booking/show.html.twig', [
            'bookingObject' => $bookingObject,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/search", name="app_booking_search")
     * @Method("POST")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return JsonResponse|Response
     */
    public function searchAction(Request $request, ApiContext $apiContext)
    {
        if ($request->isMethod("POST")) {
            $bookingObjects = [];
            $error = null;
            $name = $request->get("name");
            $priceFrom = intval($request->get("priceFrom"));
            $priceTo = intval($request->get("priceTo"));
            $typeObject = $request->get("typeObject");
            $dopField = $request->get("dopField");

            switch ($typeObject) {
                case "сottage":
                    $typeObject = "сottage";
                    break;
                default:
                    $typeObject = "pension";
                    break;
            }

            try {
                $data = [
                  'name' => $name,
                  'priceFrom' => $priceFrom,
                  'priceTo' => $priceTo,
                  'typeObject' => $typeObject,
                  'dopField' => $dopField,
                ];
                $result = $apiContext->getSearchBookingObjects($data);
                $bookingObjects = $result['result'];
            } catch (ApiException $e) {
                $error = $e->getMessage();
            }

            $view = $this->render("booking/search.html.twig",[
                'bookingObjectsResult' => $bookingObjects
            ])->getContent();

            return new JsonResponse([
                'result' => $view,
                'message' => $error
            ]);
        } else {
            return $this->redirectToRoute("homePage");
        }
    }

    /**
     * @Route("/add", name="app_booking_add")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     */
    public function addAction(Request $request, ApiContext $apiContext)
    {
        $error = null;
        $message = null;

        $formBooking = $this->createForm(BookingObjectType::class);
        $formBooking->handleRequest($request);

        $formCottage = $this->createForm(CottageType::class);
        $formCottage->handleRequest($request);

        $formPension = $this->createForm(PensionType::class);
        $formPension->handleRequest($request);

        if ($formBooking->isSubmitted() && $formBooking->isValid()) {
            $BookingObjectData = $formBooking->getData();

            switch ($BookingObjectData['type']) {
                case 'pension':
                    $typeView = 'pension';
                    $formPension->setData([
                        'BookingObjectData' => json_encode($BookingObjectData)
                    ]);
                    $formType = $formPension->createView();

                    break;
                case 'сottage':
                    $typeView = 'сottage';
                    $formCottage->setData([
                        'BookingObjectData' => json_encode($BookingObjectData)
                    ]);
                    $formType = $formCottage->createView();
                    break;
                default:
                    $typeView = 'booking_object';
                    $formType = $formBooking->createView();
                    break;
            }

            return $this->render('booking/add/'.$typeView.'.html.twig', [
                'formType' => $formType
            ]);

        }

        if ($formPension->isSubmitted() && $formPension->isValid()) {
            $result = $this->createBookingObject($formPension->getData(), $apiContext);

            if ($result['status']) {
                return $this->redirectToRoute("homePage");
            }
        }

        if ($formCottage->isSubmitted() && $formCottage->isValid()) {
            $result = $this->createBookingObject($formCottage->getData(), $apiContext);

            if ($result['status']) {
                return $this->redirectToRoute("homePage");
            }
        }

        return $this->render('booking/add/booking_object.html.twig', [
            'formBooking' => $formBooking->createView()
        ]);
    }

    public function createBookingObject($formData, ApiContext $apiContext)
    {
        $error = null;
        $status = true;
        /** @var User $user */
        $user = $this->getUser();
        try {
            $bookingObjectData = json_decode($formData['BookingObjectData'], true);
            unset($formData['BookingObjectData']);

            $data = [
                'BookingObjectData' => $bookingObjectData,
                'dopField' => $formData,
                'email' => $user->getEmail()
            ];

            $apiContext->createBookingObject($data);

        } catch (ApiException $e) {
            $status = false;
            $error = $e->getMessage();
        }

        return [
            'error' => $error,
            'status' => $status
        ];
    }

}