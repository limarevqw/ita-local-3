<?php

namespace App\Controller;

use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractBaseController
{

    /**
     * @Route("/", name="homePage")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     */
    public function indexAction(Request $request, ApiContext $apiContext)
    {
        $error = null;
        $bookingObjects = [];

        try {
            $result = $apiContext->getAllBookingObjects();
            $bookingObjects = $result['result'];
        } catch (ApiException $e) {
            $error = $e->getMessage();
        }

        $paginator = $this->get('knp_paginator');

        $bookingObjectsResult = $paginator->paginate(
            $bookingObjects,
            $request->query->getInt("page", 1),
            $request->query->getInt("limit", 6)
        );

        return $this->render('index/index.html.twig', [
            'bookingObjectsResult' => $bookingObjectsResult,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/ping", name="ping")
     * @param ApiContext $apiContext
     * @return Response
     */
    public function pingAction(ApiContext $apiContext)
    {
        try {

            return new Response(var_export($apiContext->makePing(), true));
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }

}
