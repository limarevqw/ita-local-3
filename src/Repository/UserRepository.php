<?php

namespace App\Repository;

use App\Entity\User;
use App\Lib\Enumeration\Ulogin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findOneByPasswordAndEmail(string $password, string $email)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->andWhere('a.email = :email')
                ->setParameter('password', $password)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByUid(string $uid, $network = null)
    {
        try {

            $result = $this->createQueryBuilder('u')
                ->select('u')
            ;

            if ($network == Ulogin::FACEBOOK) {
                $result->andWhere("u.faceBookId = :uid");
            } else if ($network == Ulogin::VKONTAKTE) {
                $result->andWhere("u.vkId = :uid");
            } else if ($network == Ulogin::GOOGLE) {
                $result->andWhere("u.googleId = :uid");
            }

            return $result
                ->setParameter("uid", $uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByEmail(string $email) : ?User
    {
        try {
            return $this->createQueryBuilder('u')
                ->select('u')
                ->andWhere('u.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByPassportAndEmail(string $passport, string $email) : ?User
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.passport = :passport')
                ->orWhere('a.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
