<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass_1234',
            'roles' => ["ROLE_USER","ROLE_TENANT"]
        ]);

        $manager->persist($user);
        $manager->flush();
    }
}
