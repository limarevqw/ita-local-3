<?php

namespace App\Lib;


use App\Entity\User;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;

class HelperFormType
{

    public function UserRolesTransformer(FormBuilderInterface $builder)
    {
        /** @var User $user */
        $user = $builder->getData();

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($storedRoles) use (&$user) {
                    $currentRoles = $user->getRoles();
                    if (in_array('ROLE_TENANT', $currentRoles)) {
                        return 'tenant';
                    } elseif (in_array('ROLE_LANDLORD', $currentRoles)) {
                        return 'landlord';
                    } else {
                        return null;
                    }
                },
                function ($rolesAsString) use (&$user) {
                    $currentRoles = array_diff($user->getRoles(), [
                        'ROLE_TENANT',
                        'ROLE_LANDLORD',
                    ]);

                    switch ($rolesAsString) {
                        case 'tenant':
                            $currentRoles[] = 'ROLE_TENANT';
                            break;
                        case 'landlord':
                            $currentRoles[] = 'ROLE_LANDLORD';
                            break;
                        default:
                            $currentRoles = $user->getRoles();
                            break;
                    }

                    return $currentRoles;
                }
            ));
    }

}