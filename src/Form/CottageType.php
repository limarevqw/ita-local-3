<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class CottageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Kitchen', CheckboxType::class, [
                'required' => false
            ])
            ->add('bathroom', CheckboxType::class, [
                'required' => false
            ])
            ->add('jacuzzi', CheckboxType::class, [
                'required' => false
            ])
            ->add('pool', CheckboxType::class, [
                'required' => false
            ])
            ->add('garage', CheckboxType::class, [
                'required' => false
            ])
            ->add('BookingObjectData', HiddenType::class)
            ->add('add', SubmitType::class)
        ;
    }
}