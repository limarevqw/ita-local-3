<?php

namespace App\Form;

use App\Lib\HelperFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UloginRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('passport', TextType::class)
            ->add('roles', ChoiceType::class, [
                'placeholder' => 'Выберите кто вы?',
                'choices' => [
                    'Арендатор' => 'tenant',
                    'Арендодатель' => 'landlord'
                ]

            ])
            ->add('save', SubmitType::class)
        ;

        $helperFormType = new HelperFormType();
        $helperFormType->UserRolesTransformer($builder);
    }
}