<?php
namespace App\Form;

use App\Lib\HelperFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('passport')
            ->add('password', PasswordType::class)
            ->add('roles', ChoiceType::class, [
                'placeholder' => 'Выберите кто вы?',
                'choices' => [
                    'Арендатор' => 'tenant',
                    'Арендодатель' => 'landlord'
                ]

            ])
            ->add('save', SubmitType::class)
        ;

        $helperFormType = new HelperFormType();
        $helperFormType->UserRolesTransformer($builder);
    }
}
