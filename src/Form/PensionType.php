<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PensionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('shower', CheckboxType::class, [
                'required' => false
            ])
            ->add('bunkBeds', CheckboxType::class, [
                'required' => false
            ])
            ->add('BigTV', CheckboxType::class, [
                'required' => false
            ])
            ->add('BookingObjectData', HiddenType::class)
            ->add('add', SubmitType::class)
        ;
    }
}