<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class BookingObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('numberRooms', NumberType::class)
            ->add('contactPerson')
            ->add('phone')
            ->add('address')
            ->add('pricePerDay')
            ->add('lat', HiddenType::class)
            ->add('long', HiddenType::class)
            ->add('type', ChoiceType::class, [
                'placeholder' => 'Выберите тип обьекта.',
                'choices' => [
                    'Пансионат' => 'pension',
                    'Коттедж' => 'сottage'
                ]

            ])
            ->add('more', SubmitType::class)
        ;
    }

}